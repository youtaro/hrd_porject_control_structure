import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './ProjectDivision.styles';

class ProjectDivision extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('ProjectDivision will mount');
  }

  componentDidMount = () => {
    console.log('ProjectDivision mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('ProjectDivision will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('ProjectDivision will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('ProjectDivision did update');
  }

  componentWillUnmount = () => {
    console.log('ProjectDivision will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="ProjectDivisionWrapper">
        Test content
      </div>
    );
  }
}

ProjectDivision.propTypes = {
  // bla: PropTypes.string,
};

ProjectDivision.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProjectDivision);
