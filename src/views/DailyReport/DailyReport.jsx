import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './DailyReport.styles';

class DailyReport extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('DailyReport will mount');
  }

  componentDidMount = () => {
    console.log('DailyReport mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('DailyReport will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('DailyReport will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('DailyReport did update');
  }

  componentWillUnmount = () => {
    console.log('DailyReport will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="DailyReportWrapper">
        Test content
      </div>
    );
  }
}

DailyReport.propTypes = {
  // bla: PropTypes.string,
};

DailyReport.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DailyReport);
