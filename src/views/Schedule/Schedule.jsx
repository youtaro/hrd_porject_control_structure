import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './Schedule.styles';

class Schedule extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Schedule will mount');
  }

  componentDidMount = () => {
    console.log('Schedule mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Schedule will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Schedule will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Schedule did update');
  }

  componentWillUnmount = () => {
    console.log('Schedule will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="ScheduleWrapper">
        Test content
      </div>
    );
  }
}

Schedule.propTypes = {
  // bla: PropTypes.string,
};

Schedule.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Schedule);
