import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './Task.styles';

class Task extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('Task will mount');
  }

  componentDidMount = () => {
    console.log('Task mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('Task will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('Task will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('Task did update');
  }

  componentWillUnmount = () => {
    console.log('Task will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="TaskWrapper">
        Test content
      </div>
    );
  }
}

Task.propTypes = {
  // bla: PropTypes.string,
};

Task.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Task);
