import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//import { Test } from './UserControl.styles';

class UserControl extends PureComponent { 
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentWillMount = () => {
    console.log('UserControl will mount');
  }

  componentDidMount = () => {
    console.log('UserControl mounted');
  }

  componentWillReceiveProps = (nextProps) => {
    console.log('UserControl will receive props', nextProps);
  }

  componentWillUpdate = (nextProps, nextState) => {
    console.log('UserControl will update', nextProps, nextState);
  }


  componentDidUpdate = () => {
    console.log('UserControl did update');
  }

  componentWillUnmount = () => {
    console.log('UserControl will unmount');
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }
    return (
      <div className="UserControlWrapper">
        Test content
      </div>
    );
  }
}

UserControl.propTypes = {
  // bla: PropTypes.string,
};

UserControl.defaultProps = {
  // bla: 'test',
};

const mapStateToProps = state => ({
  // blabla: state.blabla,
});

const mapDispatchToProps = dispatch => ({
  // fnBlaBla: () => dispatch(action.name()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserControl);
